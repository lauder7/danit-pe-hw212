// Initialization
document.addEventListener("keydown", hndKeyDown);



function hndKeyDown(ev) {
    let debtn = document.querySelector(`[data-keycode="${ev.code}"]`);
    if(debtn){
        let deprev = document.querySelector(".lastpressed");
        if(deprev) deprev.classList.remove("lastpressed");
        debtn.classList.add("lastpressed");
    }
}